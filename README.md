#The streams maven repository

Hosts artifacts and snapshots for use in other streams related projects
Heres the command to install stream projects into this repository

### streams-api

mvn install:install-file  -DgroupId=org.jwall -DartifactId=streams-api -Dversion=0.9.23-SNAPSHOT -Dfile=./streams/stream-api/target/streams-api-0.9.23-SNAPSHOT.jar -DlocalRepositoryPath=./streams-repo/ -Dpackaging=jar


### streams-core

mvn install:install-file  -DgroupId=org.jwall -DartifactId=streams-core -Dversion=0.9.23-SNAPSHOT -Dfile=./streams/stream-core/target/streams-core-0.9.23-SNAPSHOT.jar -DlocalRepositoryPath=./streams-repo/ -Dpackaging=jar

### streams-runtime

mvn install:install-file  -DgroupId=org.jwall -DartifactId=streams-runtime -Dversion=0.9.23-SNAPSHOT -Dfile=./streams/stream-runtime/target/streams-runtime-0.9.23-SNAPSHOT.jar -DlocalRepositoryPath=./streams-repo/ -Dpackaging=jar

### streams-storm

mvn install:install-file  -DgroupId=de.sfb876 -DartifactId=streams-runtime -Dversion=0.9.23-SNAPSHOT -Dfile=./streams-storm/target/streams-storm-0.9.23-SNAPSHOT.jar -DlocalRepositoryPath=./streams-repo/ -Dpackaging=jar